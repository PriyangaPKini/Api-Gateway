const http = require('http');
const httpProxy = require('http-proxy');
const jwt = require('jsonwebtoken');
const proxy = httpProxy.createProxyServer({});

let EventHost = 'http://localhost:8000';
let SecretKey = 'Super Secret Key';
let EventsKey = 'Events Super Secret Key';

if (process.env.ENV == 'Production') {
  EventHost = process.env.EventHost;
  SecretKey = process.env.SecretKey;
  EventsKey = process.env.EventsKey;
}

proxy.on('proxyReq', async function (proxyReq, req, res, options) {
  proxyReq.setHeader('SecretKey', EventsKey);
  if (req.headers.authorization) {
    let token = req.headers.authorization.substr(7);
    await jwt.verify(token, SecretKey, function (err, decoded) {
      if (err) {
        res.statusCode = 401;
        res.write('Jwt validation Failed');
        res.end();
      } else {
        proxyReq.setHeader('authorization', JSON.stringify(decoded));
      }
    });
  }
});

function getTarget(url) {
  if (url.startsWith('/events')) {
    return EventHost;
  }
  return 'http://localhost:1000';
}

var server = http.createServer(async function (req, res) {
  proxy.web(req, res, { target: getTarget(req.url) }, function (err) {
    res.statusCode = 500;
    res.write(err.message);
    res.end();
  });
});

console.log('Listening on 80');
server.listen(80);
