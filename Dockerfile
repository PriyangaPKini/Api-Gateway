FROM node:13.5.0-alpine
WORKDIR /gateway
COPY ./gateway/package.json ./
COPY ./gateway/yarn.lock ./
RUN yarn
COPY ./gateway/. .
CMD ["yarn", "start"]